package adventOfCode2023.day1

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class Day1Should {

    @Test
    fun `should return first and last int in a string`() {
        val result = Day1(listOf("1abc2")).computeCalibrationValue()

        assertEquals(12, result.first())
    }

    @Test
    fun `should return first and last int in a string when it is the same value`() {
        val result = Day1(listOf("treb7uchet")).computeCalibrationValue()

        assertEquals(77, result.first())
    }
}