package com.orange.ccmd.edithor.backend.aoc.day1

import adventOfCode2021.day4.Day4
import adventOfCode2021.day4.Grid
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class Day4Should {
    companion object {
        val bingoSystem = listOf(
            "7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1",
            "",
            "22 13 17 11  0",
            "8  2 23  4 24",
            "21  9 14 16  7",
            "6 10  3 18  5",
            "1 12 20 15 19",
            "",
            "3 15  0  2 22",
            "9 18 13 17  5",
            "19  8  7 25 23",
            "20 11 10 24  4",
            "14 21 16 12  6",
            "",
            "14 21 17 24  4",
            "10 16 15  9 19",
            "18  8 23 26 20",
            "22 11 13  6  5",
            "2  0 12  3  7"
        )
    }

    @Test
    fun shouldReadDrawsListFromBingoSystem() {
        val draws = Day4.readDrawsFromBingoSystem(bingoSystem)

        assertEquals(7, draws.first())
        assertEquals(27, draws.count())
    }

    @Test
    fun shouldReadDrawListFromBingoSystem() {
        val grids = Day4.readGridsFromBingoSystem(bingoSystem, 5)

        assertEquals(3, grids.size)
        assertEquals(25, grids[0].size)
        assertEquals(22, grids[0].first())
        assertEquals(19, grids[0].last())

        assertEquals(25, grids[1].size)
        assertEquals(3, grids[1].first())
        assertEquals(6, grids[1].last())

        assertEquals(25, grids[2].size)
        assertEquals(14, grids[2].first())
        assertEquals(7, grids[2].last())
    }

    @Test
    fun shouldFillBoardAndFillHorizontalBingo() {
        val grids = Day4.readGridsFromBingoSystem(bingoSystem, 5)

        val board = Grid(5, 1, grids.first())

        board.markCell(22)
        assertFalse(board.isBingoOnHorizontal())

        board.markCell(13)
        assertFalse(board.isBingoOnHorizontal())

        board.markCell(17)
        assertFalse(board.isBingoOnHorizontal())

        board.markCell(11)
        assertFalse(board.isBingoOnHorizontal())

        board.markCell(0)
        assertTrue(board.isBingoOnHorizontal())
    }

    @Test
    fun shouldFillBoardAndFillVerticalBingo() {
        val grids = Day4.readGridsFromBingoSystem(bingoSystem, 5)

        val board = Grid(5, 1, grids.first())

        board.markCell(22)
        assertFalse(board.isBingoOnVertical())

        board.markCell(8)
        assertFalse(board.isBingoOnVertical())

        board.markCell(21)
        assertFalse(board.isBingoOnVertical())

        board.markCell(6)
        assertFalse(board.isBingoOnVertical())

        board.markCell(1)
        assertTrue(board.isBingoOnVertical())
    }

    @Test
    fun shouldPlay() {
        val intGrids = Day4.readGridsFromBingoSystem(bingoSystem, 5)
        val draws = Day4.readDrawsFromBingoSystem(bingoSystem)
        val grids = Day4.buildGrids(intGrids)

        val result = Day4.play(draws, grids)

        assertEquals(4512, result)
    }

    @Test
    fun shouldLoose() {
        val intGrids = Day4.readGridsFromBingoSystem(bingoSystem, 5)
        val draws = Day4.readDrawsFromBingoSystem(bingoSystem)
        val grids = Day4.buildGrids(intGrids)

        val result = Day4.loose(draws, grids)

        assertEquals(1924, result)
    }
}
