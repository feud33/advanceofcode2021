package com.orange.ccmd.edithor.backend.aoc.day1

import adventOfCode2021.day7.Day7
import adventOfCode2021.day7.Day7.Companion.median
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class Day7Should {
    companion object {
        val crabs = listOf(16, 1, 2, 0, 4, 2, 7, 1, 2, 14)
    }


    @Test
    fun returns() {
        val median = crabs.median()

        assertEquals(37, crabs.map { kotlin.math.abs(it - median) }.sum())
    }

    @Test
    fun returnsNewFuelConsumptionRate() {
        assertEquals(168, Day7.fuelConsumptionNewRate(crabs))
    }

}
