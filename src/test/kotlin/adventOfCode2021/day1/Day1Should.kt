package adventOfCode2021.day1

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class Day1Should {
    companion object {
        val report = listOf(
            199,
            200,
            208,
            210,
            200,
            207,
            240,
            269,
            260,
            263
        )
    }

    @Test
    fun shouldReturn7() {
        val result = Day1.countNumberOfTimesDepthMeasurementIncreases(report)

        assertEquals(7, result)
    }

    @Test
    fun shouldReturnSlidingResult7() {
        val result = Day1.countNumberOfTimesSumOfMeasurementsInSlidingWindowIncreases(report)

        assertEquals(5, result)
    }
}