package com.orange.ccmd.edithor.backend.aoc.day1

import adventOfCode2021.day6.Day6
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class Day6Should {
    companion object {
        val fishes = listOf(3, 4, 3, 1, 2)
        val initialValue = 1
    }

    @Test
    fun return1For1Day() {
        assertEquals(1, Day6.singleLanternFishLife(initialValue, 1))
    }

    @Test
    fun return2For2Day() {
        assertEquals(2, Day6.singleLanternFishLife(initialValue, 2))
    }

    @Test
    fun return2For8Day() {
        assertEquals(2, Day6.singleLanternFishLife(initialValue, 8))
    }

    @Test
    fun return3For9Day() {
        assertEquals(3, Day6.singleLanternFishLife(initialValue, 9))
    }

    @Test
    fun return3For10Day() {
        assertEquals(3, Day6.singleLanternFishLife(initialValue, 10))
    }

    @Test
    fun return4For11Day() {
        assertEquals(4, Day6.singleLanternFishLife(initialValue, 11))
    }

    @Test
    fun return4For15Day() {
        assertEquals(4, Day6.singleLanternFishLife(initialValue, 15))
    }

    @Test
    fun return5For16Day() {
        assertEquals(5, Day6.singleLanternFishLife(initialValue, 16))
    }

    @Test
    fun return5For17Day() {
        assertEquals(5, Day6.singleLanternFishLife(initialValue, 17))
    }

    @Test
    fun return7For18Day() {
        assertEquals(7, Day6.singleLanternFishLife(initialValue, 18))
    }

    @Test
    fun listOfFishForfishes2days() {
        assertEquals(26, Day6.numberOfLanternfishAfterNDays(fishes, 18))
    }

    @Test
    fun listOfFishAfter256days() {
        // assertEquals(26984457539, Day6.numberOfLanternfishAfterNDays(fishes, 256))
    }
}
