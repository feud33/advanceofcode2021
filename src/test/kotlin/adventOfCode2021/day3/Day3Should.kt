package com.orange.ccmd.edithor.backend.aoc.day1

import adventOfCode2021.day3.Day3
import adventOfCode2021.day3.DiagnosticReport
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class Day3Should {
    companion object {
        val diagnosticReport = listOf(
            "00100",
            "11110",
            "10110",
            "10111",
            "10101",
            "01111",
            "00111",
            "11100",
            "10000",
            "11001",
            "00010",
            "01010",
        )
    }

    @Test
    fun shouldCheckGamaAndEpsilonRate() {
        val powerConsumptionReport = DiagnosticReport(4)
        powerConsumptionReport.addDiagnostic("1010")

        val gammaRate = powerConsumptionReport.mostCommonBits().toInt(2)
        assertEquals(10, gammaRate)
        val epsilonRate = powerConsumptionReport.lessCommonBits().toInt(2)
        assertEquals(5, epsilonRate)
    }

    @Test
    fun shouldComputeO2AndCO2Rate() {
        val o2Rating = Day3.filterLifeSupportRating(diagnosticReport, 0, true).first().toInt(2)
        val co2Rating = Day3.filterLifeSupportRating(diagnosticReport, 0, false).first().toInt(2)
        val ratingResult = Day3.computeLifeSupportRating(diagnosticReport)

        assertEquals(23, o2Rating)
        assertEquals(10, co2Rating)
        assertEquals(230, ratingResult)
    }
}
