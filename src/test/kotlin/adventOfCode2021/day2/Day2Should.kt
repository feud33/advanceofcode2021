package com.orange.ccmd.edithor.backend.aoc.day1

import adventOfCode2021.day2.Command
import adventOfCode2021.day2.Day2
import adventOfCode2021.day2.toCommand
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class Day2Should {
    companion object {
        val plannedCourse = listOf(
            "forward 5",
            "down 5",
            "forward 8",
            "up 3",
            "down 8",
            "forward 2",
        )
    }

    @Test
    fun shouldFindForwardAndUnit() {
        val commandeLine = "forward 5"

        val command = commandeLine.toCommand()

        assertEquals(command.direction, Command.Direction.FORWARD)
        assertEquals(command.unit, 5)
    }

    @Test
    fun shouldVerifyMoving() {
        val position = Day2.computePosition(plannedCourse)

        assertEquals(15, position.horizontal)
        assertEquals(10, position.depth)
    }

    @Test
    fun shouldVerifyMovingWithAim() {
        val position = Day2.computePositionWithAim(plannedCourse)

        assertEquals(15, position.horizontal)
        assertEquals(60, position.depth)
    }
}
