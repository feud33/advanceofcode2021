package com.orange.ccmd.edithor.backend.aoc.day1

import adventOfCode2021.day5.Board
import adventOfCode2021.day5.Day5
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class Day5Should {
    companion object {
        val horinzontalHydrothermalVents = listOf(
            "0,9 -> 5,9",
            "9,4 -> 3,4",
            "2,2 -> 2,1",
            "7,0 -> 7,4",
            "0,9 -> 2,9",
            "3,4 -> 1,4"
        )

        val hydrothermalVents = listOf(
            "0,9 -> 5,9",
            "8,0 -> 0,8",
            "9,4 -> 3,4",
            "2,2 -> 2,1",
            "7,0 -> 7,4",
            "6,4 -> 2,0",
            "0,9 -> 2,9",
            "3,4 -> 1,4",
            "0,0 -> 8,8",
            "5,5 -> 8,2",
        )
    }

    @Test
    fun shouldReadHydrothermalVents() {
        val segments = Day5.readHydrothermalVents(hydrothermalVents)

        assertEquals(0, segments.first().sourceCoordinate.x)
        assertEquals(9, segments.first().sourceCoordinate.y)
        assertEquals(5, segments.first().targetCoordinate.x)
        assertEquals(9, segments.first().targetCoordinate.y)
    }

    @Test
    fun shouldFindValueForHorizontalVerticalVents() {
        val segments = Day5.readHydrothermalVents(horinzontalHydrothermalVents)

        val board = Board(10)
        val result = board.markSegments(segments).toList().filter { it >= 2 }.count()

        assertEquals(5, result)
    }

    @Test
    fun shouldFindValueHydrothermalVents() {
        val segments = Day5.readHydrothermalVents(hydrothermalVents)

        val board = Board(10)
        val result = board.markSegments(segments).toList().filter { it >= 2 }.count()

        assertEquals(12, result)
    }
}
