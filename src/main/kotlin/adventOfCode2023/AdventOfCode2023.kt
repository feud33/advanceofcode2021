package adventOfCode2023

import adventOfCode2023.day1.Day1
import java.io.File

class AdventOfCode2023 {

    companion object {
        fun runAdventOfCode() {
            val calibrationDocumentLines = File("src/main/resources/2023/day1.data").readLines()
            println("Day1 : " + Day1(calibrationDocumentLines).computeCalibrationValue().sum())
        }
    }
}
