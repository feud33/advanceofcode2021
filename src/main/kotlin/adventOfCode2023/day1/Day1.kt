package adventOfCode2023.day1

class Day1(
    private val calibrationDocumentLines: List<String>
) {
    fun computeCalibrationValue(): List<Int> {
        return calibrationDocumentLines.map { calibrationLine ->
            val firstCalibrationIntValue = calibrationLine.map { it }.filter {
                it.isDigit()
            }.map { it.toString().toInt() }.first()
            val lastCalibrationIntValue = calibrationLine.map { it }.filter {
                it.isDigit()
            }.map { it.toString().toInt() }.last()

            firstCalibrationIntValue * 10 + lastCalibrationIntValue
        }
    }
}