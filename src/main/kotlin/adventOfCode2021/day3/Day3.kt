package adventOfCode2021.day3

class Day3 {

    companion object {
        fun computePowerConsumption(diagnostics: List<String>): Int {
            val diagnosticReport = computeDiagnosticReport(diagnostics)
            val gammaRate = diagnosticReport.mostCommonBits().toInt(2)
            val epsilonRate = diagnosticReport.lessCommonBits().toInt(2)

            return gammaRate * epsilonRate
        }

        fun computeLifeSupportRating(diagnostics: List<String>): Int {
            val o2Rating = filterLifeSupportRating(diagnostics, 0, true).first().toInt(2)
            val co2Rating = filterLifeSupportRating(diagnostics, 0, false).first().toInt(2)

            return o2Rating * co2Rating
        }

        fun filterLifeSupportRating(diagnostics: List<String>, position: Int, mostCommon: Boolean): List<String> {
            val diagnosticReport = computeDiagnosticReport(diagnostics)
            val filterDiagnostics = diagnostics.filter { diagnosticReport.isMostCommonBit(it, position, mostCommon) }
            return if (filterDiagnostics.count() == 1) {
                filterDiagnostics
            } else {
                filterLifeSupportRating(filterDiagnostics, position + 1, mostCommon)
            }
        }

        private fun computeDiagnosticReport(diagnostics: List<String>): DiagnosticReport {
            val diagnosticReport = DiagnosticReport(diagnostics.first().length)
            diagnostics.fold(diagnosticReport) { report, element ->
                report.addDiagnostic(element)
            }
            return diagnosticReport
        }

    }
}