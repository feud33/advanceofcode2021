package adventOfCode2021.day3

class DiagnosticReport(
    val bitSize: Int = 5
) {
    private var lineCount: Int = 0
    private val numbersOf1: MutableList<Int> = (0 until this.bitSize).map { 0 }.toMutableList()

    private val mostCommonBit = { number: Int, maxNumber: Int -> (number >= (maxNumber - number)) }
    private val lessCommonBit = { number: Int, maxNumber: Int -> (number < (maxNumber - number)) }


    fun addDiagnostic(element: String): DiagnosticReport {
        element.toCharArray().forEachIndexed { index, c ->
            if (c == '1') {
                this.numbersOf1[index]++
            }
        }
        this.lineCount++
        return this
    }

    private fun Int.toBit(mostCommon: (Int, Int) -> Boolean, max: Int): String {
        return if (mostCommon(this, max)) "1" else "0"
    }


    fun mostCommonBits(): String {
        return this.numbersOf1.joinToString(separator = "") { it.toBit(this.mostCommonBit, this.lineCount) }
    }

    fun lessCommonBits(): String {
        return this.numbersOf1.joinToString(separator = "") { it.toBit(this.lessCommonBit, this.lineCount) }
    }

    fun isMostCommonBit(bits: String, position: Int, mostCommon: Boolean): Boolean {
        val commonBitLambda = if (mostCommon) this.mostCommonBit else this.lessCommonBit
        return this.numbersOf1[position].toBit(commonBitLambda, this.lineCount) == bits[position].toString()
    }
}

