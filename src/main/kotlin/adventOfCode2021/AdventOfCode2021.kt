package adventOfCode2021

import adventOfCode2021.day1.Day1
import adventOfCode2021.day2.Day2
import adventOfCode2021.day3.Day3
import adventOfCode2021.day4.Day4
import adventOfCode2021.day5.Board
import adventOfCode2021.day5.Day5
import adventOfCode2021.day6.Day6
import adventOfCode2021.day7.Day7
import adventOfCode2021.day8.Day8
import java.io.File

class AdventOfCode2021 {

    companion object {
        fun runAdventOfCode() {
            val report = File("src/main/resources/2021/day1.data").readLines().map { it.toInt() }
            println(
                "adventOfCode2021.day1.Day1 part 1 result : ${
                    Day1.countNumberOfTimesDepthMeasurementIncreases(
                        report
                    )
                }"
            )
            println(
                "adventOfCode2021.day1.Day1 part 2 result : ${
                    Day1.countNumberOfTimesSumOfMeasurementsInSlidingWindowIncreases(
                        report
                    )
                }"
            )


            val plannedCourse = File("src/main/resources/2021/day2.data").readLines()
            val position = Day2.computePosition(plannedCourse)
            println("Day2 part 1 result : ${position.depth * position.horizontal}") // 1250395
            val positionWithAim = Day2.computePositionWithAim(plannedCourse)
            println("Day2 part 2 result : ${positionWithAim.depth * positionWithAim.horizontal}") // 1451210346

            val diagnosticReport = File("src/main/resources/2021/day3.data").readLines()
            println("Day3 part 1 result : ${Day3.computePowerConsumption(diagnosticReport)}")
            println("Day3 part 2 result : ${Day3.computeLifeSupportRating(diagnosticReport)}")

            val bingoSystem = File("src/main/resources/2021/day4.data").readLines()
            val intGrids = Day4.readGridsFromBingoSystem(bingoSystem, 5)
            val draws = Day4.readDrawsFromBingoSystem(bingoSystem)
            val grids = Day4.buildGrids(intGrids)
            println("Day4 part 1 result : ${Day4.play(draws, grids)}")
            val drawsPart2 = Day4.readDrawsFromBingoSystem(bingoSystem)
            val gridsPart2 = Day4.buildGrids(intGrids)
            println("Day4 part 2 result : ${Day4.loose(drawsPart2, gridsPart2)}")

            val hydrothermalVents = File("src/main/resources/2021/day5.data").readLines()
            val segments = Day5.readHydrothermalVents(hydrothermalVents)
                .filter { it.sourceCoordinate.x == it.targetCoordinate.x || it.sourceCoordinate.y == it.targetCoordinate.y }
            println("Day5 part 1 result : ${Board().markSegments(segments).toList().filter { it >= 2 }.count()}")
            val fullSegments = Day5.readHydrothermalVents(hydrothermalVents)
            println("Day5 part 2 result : ${Board().markSegments(fullSegments).toList().filter { it >= 2 }.count()}")

            val lanternFishes =
                File("src/main/resources/2021/day6.data").readLines().first().split(",").map { it.toInt() }
            print("Day6 part 1 [")
            println("] result : ${Day6.numberOfLanternfishAfterNDays(lanternFishes, 80)}") //80 -> 350149
            print("Day6 part 2 [")
            println("] result : ${Day6.numberOfLanternfishAfterNDays(lanternFishes, 200)}") // 256 -> 1590327954513

            val crabs = File("src/main/resources/2021/day7.data").readLines().first().split(",").map { it.toInt() }
            println("Day7 part 1 result : ${Day7.fuelConsumption(crabs)}")
            println("Day7 part 2 result : ${Day7.fuelConsumptionNewRate(crabs)}")

            val digitSegments = File("src/main/resources/2021/day8.data").readLines()
            println("Day8 part 1 result : ${Day8.onlyDigitsOutputValues(digitSegments)}")
        }
    }
}