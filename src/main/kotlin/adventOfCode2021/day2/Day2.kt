package adventOfCode2021.day2

class Day2 {

    companion object {
        fun computePosition(commands: List<String>): Position {
            val position = Position()
            return commands
                .map { it.toCommand() }
                .fold(position) { pos, element ->
                    pos.move(element.direction, element.unit)
                }
        }

        fun computePositionWithAim(commands: List<String>): Position {
            val position = Position()
            return commands
                .map { it.toCommand() }
                .fold(position) { pos, element ->
                    pos.moveWithAim(element.direction, element.unit)
                }
        }
    }
}

fun String.toCommand(): Command {
    val args = this.split(" ")
    require(args.size == 2)
    val direction = Command.Direction.of(args.first())
    val unit = args.last().toInt()
    return Command(direction, unit)
}