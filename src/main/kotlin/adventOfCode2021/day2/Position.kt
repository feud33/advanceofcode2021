package adventOfCode2021.day2

data class Position(
    val horizontal: Int = 0,
    val depth: Int = 0,
    val aim: Int = 0
) {
    fun move(direction: Command.Direction, unit: Int): Position {
        return when (direction) {
            Command.Direction.FORWARD -> Position(this.horizontal + unit, this.depth)
            Command.Direction.UP -> Position(this.horizontal, this.depth - unit)
            Command.Direction.DOWN -> Position(this.horizontal,this.depth + unit)
        }
    }

    fun moveWithAim(direction: Command.Direction, unitQuantity: Int): Position {
        return when (direction) {
            Command.Direction.DOWN -> Position( this.horizontal, this.depth, this.aim + unitQuantity)
            Command.Direction.UP -> Position( this.horizontal, this.depth, this.aim - unitQuantity)
            Command.Direction.FORWARD -> Position( this.horizontal + unitQuantity, this.depth + this.aim * unitQuantity, this.aim)
        }
    }
}