package adventOfCode2021.day2

data class Command(
    val direction: Direction,
    val unit: Int
) {

    enum class Direction(val value: String) {
        FORWARD("forward"),
        DOWN("down"),
        UP("up");

        companion object {
            fun of(value: String): Direction {
                val direction = values().find { it.value == value }
                requireNotNull(direction)
                return direction
            }
        }
    }


}
