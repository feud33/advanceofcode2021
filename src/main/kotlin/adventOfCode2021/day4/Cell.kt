package adventOfCode2021.day4

data class Cell(
    val number: Int,
    var marked: Boolean = false
)