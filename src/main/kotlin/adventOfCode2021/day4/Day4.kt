package adventOfCode2021.day4

class Day4 {

    companion object {
        fun readDrawsFromBingoSystem(bingoSystem: List<String>): List<Int> {
            return bingoSystem.first().split(",").map { it.toInt() }
        }

        fun readGridsFromBingoSystem(bingoSystem: List<String>, gridSize: Int): List<List<Int>> {
            return bingoSystem.drop(1)
                .filter { it.isNotBlank() }
                .windowed(gridSize, gridSize)
                .map { grid ->
                    grid.joinToString(" ").split(" ").filter { it.isNotBlank() }.map { it.toInt() }
                }
        }

        fun buildGrids(grids: List<List<Int>>): List<Grid> {
            return grids.mapIndexed { index, grid ->
                Grid(5, index, grid)
            }
        }

        fun play(draws: List<Int>, grids: List<Grid>): Int {
            var i = -1
            while (grids.none { it.isBingo() }) {
                val number = draws[++i]
                grids.forEach { it.markCell(number) }
            }

            val sumLeftNumbers = grids.filter { it.isBingo() }.map { it.computePoints() }.first()
            val drawNumber = draws[i]

            return sumLeftNumbers * drawNumber
        }

        fun loose(draws: List<Int>, grids: List<Grid>): Int {
            var i = -1
            while (grids.filter { it.isBingo() }.count() != grids.count() - 1) {
                val number = draws[++i]
                grids.forEach { it.markCell(number) }
            }

            val drawNumber = draws[++i]
            val lastGrid = grids.first { !it.isBingo() }
            lastGrid.markCell(drawNumber)

            return lastGrid.computePoints() * drawNumber
        }
    }
}