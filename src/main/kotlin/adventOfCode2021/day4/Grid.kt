package adventOfCode2021.day4

data class Grid(
    val boardSize: Int,
    val index: Int,
    val boardNumber: List<Int>
) {
    private val rowCells: List<Cell> = boardNumber.map {
        Cell(it)
    }
    private val colCells: List<Cell> = (1..this.boardSize).map { col ->
        (1..this.boardSize).map { row ->
            rowCells[(col + (row - 1) * this.boardSize) - 1]
        }
    }.flatten()

    fun markCell(number: Int) {
        rowCells.filter { cell ->
            cell.number == number
        }.forEach { cell ->
            cell.marked = true
        }
    }

    fun isBingoOnHorizontal(): Boolean {
        return isBingo(rowCells)
    }

    fun isBingoOnVertical(): Boolean {
        return isBingo(colCells)
    }

    fun isBingo(): Boolean {
        return isBingoOnHorizontal() || isBingoOnVertical()
    }

    fun computePoints(): Int {
        return rowCells.filter { !it.marked }.map { it.number }.sum()
    }

    private fun isBingo(cells: List<Cell>) = cells.windowed(this.boardSize, this.boardSize).map { row ->
        row.none { cell -> !cell.marked }
    }.any { rowMarked -> rowMarked }
}