package adventOfCode2021.day7

import kotlin.math.abs

class Day7 {

    companion object {
        fun List<Int>.median(): Int {
            val size = this.size
            val middle = size / 2
            val thisAsAnArray = this.sorted().toIntArray()
            return if (size % 2 == 1) {
                thisAsAnArray[middle]
            } else {
                ((thisAsAnArray[middle - 1] + thisAsAnArray[middle]) / 2.0).toInt()
            }
        }

        fun fuelConsumption(crabs: List<Int>): Int {
            val median = crabs.median()
            return crabs.map { abs(it - median) }.sum()
        }

        fun fuelConsumptionNewRate(crabs: List<Int>): Int {
            val min = crabs.minOrNull() ?: 0
            val max = crabs.maxOrNull() ?: 0
            return (min..max).map { currentLevel ->
                crabs.map {
                    val delta = abs(it - currentLevel)
                    (delta * (delta + 1)) / 2
                }.sum()
            }.minOrNull() ?: 0
        }
    }
}