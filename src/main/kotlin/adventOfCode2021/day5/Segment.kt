package adventOfCode2021.day5

data class Segment(
    val sourceCoordinate: Coordinate,
    val targetCoordinate: Coordinate
)
