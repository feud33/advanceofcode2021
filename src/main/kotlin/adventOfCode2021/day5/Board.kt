package adventOfCode2021.day5

class Board(
    val size: Int = 1000
) {
    val cells = Array(size) { IntArray(size) }

    fun markCell(x: Int, y: Int) {
        cells[y][x] += 1
    }

    fun toList(): List<Int> {
        return this.cells
            .map { it.map { value -> value } }
            .flatten()
    }

    fun markSegments(segments: List<Segment>): Board {
        segments.forEach { segment ->
            val x1 = minOf(segment.sourceCoordinate.x, segment.targetCoordinate.x)
            val x2 = maxOf(segment.sourceCoordinate.x, segment.targetCoordinate.x)
            val y1 = minOf(segment.targetCoordinate.y, segment.sourceCoordinate.y)
            val y2 = maxOf(segment.sourceCoordinate.y, segment.targetCoordinate.y)

            when {
                x1 == x2 -> {
                    (y1..y2).forEach { markCell(x1, it) }
                }
                y1 == y2 -> {
                    (x1..x2).forEach { markCell(it, y1) }
                }
                else -> {
                    val xInc = segment.sourceCoordinate.x incrementOf segment.targetCoordinate.x
                    val yInc = segment.sourceCoordinate.y incrementOf segment.targetCoordinate.y
                    (0..(x2 - x1)).forEach { i ->
                        markCell(segment.sourceCoordinate.x + (i * xInc), segment.sourceCoordinate.y + (i * yInc))
                    }
                }
            }

        }
        return this
    }
}

infix fun Int.incrementOf(i: Int): Int {
    return if (this > i) {
        -1
    } else {
        1
    }
}
