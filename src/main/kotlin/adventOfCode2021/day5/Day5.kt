package adventOfCode2021.day5

class Day5 {

    companion object {
        fun readHydrothermalVents(hydrothermalVents: List<String>): List<Segment> {
            //         "0,9 -> 5,9",
            return hydrothermalVents.map {
                val stringSegment = it.split(" ")
                val sourceCoordinate = stringSegment.first().toCoordinate()
                val targetCoordinate = stringSegment.last().toCoordinate()
                Segment(sourceCoordinate, targetCoordinate)
            }
        }
    }
}