package adventOfCode2021.day5

data class Coordinate(
    val x: Int,
    val y: Int
)

fun String.toCoordinate(): Coordinate {
    return Coordinate(
        x = this.split(",").first().toInt(),
        y = this.split(",").last().toInt()
    )
}