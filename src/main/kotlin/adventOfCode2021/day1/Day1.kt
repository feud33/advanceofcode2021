package adventOfCode2021.day1

class Day1 {

    companion object {
        fun countNumberOfTimesDepthMeasurementIncreases(report: List<Int>): Int {
            val pairReport = report zip report.drop(1)

            return pairReport.count { it.first < it.second }
        }

        fun countNumberOfTimesSumOfMeasurementsInSlidingWindowIncreases(report: List<Int>): Int {
            val reportSumBySlidingWindow = report.windowed(3).map {
                it.sum()
            }

            return countNumberOfTimesDepthMeasurementIncreases(reportSumBySlidingWindow)
        }
    }
}