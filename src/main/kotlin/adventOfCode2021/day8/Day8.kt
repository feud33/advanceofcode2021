package adventOfCode2021.day8

class Day8 {

    companion object {
        fun onlyDigitsOutputValues(digitSegments: List<String>): Int {
            return digitSegments.map { line ->
                line.split("|").last().split(" ").filter {
                    it.length == 2 || it.length == 3 || it.length == 4 || it.length == 7
                }
            }.flatten().count()
        }
    }
}