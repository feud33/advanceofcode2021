package adventOfCode2021.day6

class Day6 {

    companion object {
        private const val newFishLifeCycle = 8
        private const val oldFishResartLifeCycle = 6
        private const val timeToCreateABaby = 0

        fun singleLanternFishLife(initialValue: Int, numberOfDay: Long): Long {
            return if (initialValue >= numberOfDay) {
                1
            } else {
                singleLanternFishLife(initialValue, numberOfDay - 7)
                +singleLanternFishLife(initialValue + 2, numberOfDay - 7)
            }
        }

        fun numberOfLanternfishAfterNDays(lanternFishes: List<Int>, numberOfDay: Int): Long {
            return lanternFishes.groupBy { it }.map { (i, fishes) ->
                print(" $i, ")
                singleLanternFishLife(i, numberOfDay.toLong()) * fishes.count()
            }.sum()
        }
    }
}