import adventOfCode2021.AdventOfCode2021
import adventOfCode2023.AdventOfCode2023

fun main() {

    val year = 2023

    when (year) {
        2021 -> AdventOfCode2021.runAdventOfCode()
        2023 -> AdventOfCode2023.runAdventOfCode()
    }
}

